<html>
<head>
  <title></title>
  <script src="assets/js/jquery.min.js"></script>
  <script src="plugins/tinymce/tinymce.min.js"></script>
  <script type="text/javascript">
  tinymce.init({
      selector: "textarea",theme: "modern",height: 300,
         relative_urls : false,
         remove_script_host: false,
      plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
         "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
      ],
      
     toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
     toolbar2: "| responsivefilemanager | code | link unlink anchor | image media | forecolor backcolor  | print preview  ",
     image_advtab: true ,
   
     external_filemanager_path:"plugins/tinymce/plugins/filemanager/",
     filemanager_title:"Responsive Filemanager" ,
     external_plugins: { "filemanager" : "../tinymce/plugins/filemanager/plugin.min.js"}
 });
</script>

</head>
<body>



<textarea>
 
</textarea>



</body>
</html>