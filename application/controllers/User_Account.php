<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Account extends CI_Controller {

	public function __construct(){
        parent::__construct();

        if($this->session->userdata('logged_in') == null){
          redirect('Auth/login', 'refresh');
        }
        $this->user  = $this->Auth_Model->get_type_user()->type_user;
 	}

    public function index(){
        
      
        $data_section = array(
                    'id_menu'                       =>  "menu_5",
                    'submenu_id'                    =>  "submenu-5_3",
                    'this_section'                  =>  "index"
        );
        $this->session->set_userdata($data_section);

        
        if($this->session->userdata('logged_in')){
                $session_data          =   $this->session->userdata('logged_in');
                $data['username']      =   $session_data['username'];
                $data['email']         =   $session_data['email'];

                $data['user']          =   $this->user;
                $data['province']      =   $this->province_model->get();
                $data['data_user']     =   $this->Auth_Model->get_data_user();

                $data['content']       =   "User_Account/index";
                $this->load->view('Dashboard/homedashboard',$data);
        }else {
                    redirect('Auth/login', 'refresh');
        }
        
    }


	public function accountdata(){

                
        $data_section = array(
                    'id_menu'                       =>  "menu_5",
                    'submenu_id'                    =>  "submenu-5_0",
                    'this_section'                  =>  "index"
        );
        $this->session->set_userdata($data_section);

        
        if($this->session->userdata('logged_in')){
                $session_data         =   $this->session->userdata('logged_in');
                $data['username']     =   $session_data['username'];
                $data['email']        =   $session_data['email']; 

                $data['prefix']       =   array('Mr','Mrs','Ms','Miss');
                $data['province']     =   $this->province_model->get();

                $data['user']         =   $this->user;
                $data['customer']     =   $this->Auth_Model->get();
                $data['data_user']    =   $this->Auth_Model->get_data_user();

                $data['content']      =   "User_Account/accountdata";
                $this->load->view('Dashboard/homedashboard',$data);
        }else {
                    redirect('Auth/login', 'refresh');
        }
		
    }

    public function save_edit(){

      $sessce =  $this->Auth_Model->save_edit_account();
      ($sessce==TRUE?$txt_status=1:$txt_status=$this->db->error()['code']);
          echo json_encode(array("txt_status"=>"$txt_status"));
          unset($_POST['id_customer']);
          unset($_POST['name']);

    }

    public function contactdata(){

                
        $data_section = array(
                    'id_menu'                       =>  "menu_5",
                    'submenu_id'                    =>  "submenu-5_1",
                    'this_section'                  =>  "index"
        );
        $this->session->set_userdata($data_section);

        
        if($this->session->userdata('logged_in')){
                $session_data        =   $this->session->userdata('logged_in');
                $data['username']    =   $session_data['username'];
                $data['email']       =   $session_data['email']; 
                $data['user']        =   $this->user;

                
                $data['customer']    =   $this->Auth_Model->get();
                $data['data_user']   =   $this->Auth_Model->get_data_user();

                

                $data['content']     =   "User_Account/contactdata";
                $this->load->view('Dashboard/homedashboard',$data);
        }else {
                    redirect('Auth/login', 'refresh');
        }
        
    }

    public function save_edit_contact(){

      $sessce =  $this->Auth_Model->save_edit_contact();
      ($sessce==TRUE?$txt_status=1:$txt_status=$this->db->error()['code']);
          echo json_encode(array("txt_status"=>"$txt_status"));
          unset($_POST['id_customer']);
          unset($_POST['name']);

    }

    public function follow_newsletter(){

                
        $data_section = array(
                    'id_menu'                       =>  "menu_10",
                    'submenu_id'                    =>  "submenu-10_0",
                    'this_section'                  =>  "index"
        );
        $this->session->set_userdata($data_section);

        
        if($this->session->userdata('logged_in')){
                $session_data          =   $this->session->userdata('logged_in');
                $data['username']      =   $session_data['username'];
                $data['email']         =   $session_data['email']; 
                $data['user']          =   $this->user;

                $data['category']      =   $this->Category_model->get();
                $data['data_user']     =   $this->Auth_Model->get_data_user();

                $data['show_follownews']  =   $this->Auth_Model->get_follownews();


                $data['content']       =   "User_Account/follow_newsletter";
                $this->load->view('Dashboard/homedashboard',$data);
        }else {
                    redirect('Auth/login', 'refresh');
        }
        
    }

    public function save_follownews(){


        $data_section = array(
                    'id_menu'                       =>  "menu_10",
                    'submenu_id'                    =>  "submenu-10_0",
                    'this_section'                  =>  "index"
        );
        $this->session->set_userdata($data_section);

        
        if($this->session->userdata('logged_in')){
                $session_data             =   $this->session->userdata('logged_in');
                $data['username']         =   $session_data['username'];
                $data['email']            =   $session_data['email']; 
                $data['user']             =   $this->user;

                $data['category']         =   $this->Category_model->get();
                $data['data_user']        =   $this->Auth_Model->get_data_user();

                $data['follownews']       =   $this->Auth_Model->save_follownews();
                $data['show_follownews']  =   $this->Auth_Model->get_follownews();

                $data['content']          =   "User_Account/follow_newsletter";
                $this->load->view('Dashboard/homedashboard',$data);
        }else {
                    redirect('Auth/login', 'refresh');
        }              
            
    }


    public function change_password(){
   
        if($this->session->userdata('logged_in')){
                $session_data          =   $this->session->userdata('logged_in');
                $data['username']      =   $session_data['username'];
                $data['email']         =   $session_data['email']; 
                $data['user']          =   $this->user;

                $data['data_user']     =   $this->Auth_Model->get_data_user();
                $data['content']       =   "User_Account/change_password";
                $this->load->view('Dashboard/homedashboard',$data);
        }else {
                    redirect('Auth/login', 'refresh');
        }
        
    }

    public function save_change(){

        if($this->session->userdata('logged_in')){
            $session_data          =   $this->session->userdata('logged_in');
            $data['username']      =   $session_data['username'];
            $data['email']         =   $session_data['email']; 
            $data['user']          =   $this->user;

            $data['data_user']     =   $this->Auth_Model->get_data_user();

            $this->form_validation->set_rules('newpassword', 'รหัสผ่านใหม่', 'required|min_length[4]|max_length[8]');

            if ($this->form_validation->run() == FALSE){
                $data['error']             = validation_errors();
                $data['content']           =   "User_Account/change_password";
                $this->load->view('Dashboard/homedashboard',$data);
            }else{

                $data['password_fail']     = $this->Auth_Model->save_change();
                if($data['password_fail']){
                    $data['content']       =   "User_Account/change_password";
                    $this->load->view('Dashboard/homedashboard',$data);
                }else 
                if($data['password_fail'] == FALSE){
                    $data['success']       =   "เปลี่ยนรหัสผ่านสำเร็จ";
                    $data['data_user']     =   $this->Auth_Model->get_data_user();
                    $data['save_newpass']  =   $this->Auth_Model->save_change();
                    $data['content']       =   "User_Account/index";
                    $this->load->view('Dashboard/homedashboard',$data);
                }
            }
        }else {
                redirect('Auth/login', 'refresh');
        }

    }

    
		
}