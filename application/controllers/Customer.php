<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function __construct(){
        parent::__construct();

        if($this->session->userdata('logged_in') == null){
          redirect('Auth/login', 'refresh');
        }
        $this->user  = $this->Auth_Model->get_type_user()->type_user;
        
 	}

	public function index(){

		$data_section = array(
					'id_menu'						=>	"menu_4",
					'submenu_id'					=>	"submenu-4_0",
					'this_section'					=>	"index"
		);
		$this->session->set_userdata($data_section);

		
        if($this->session->userdata('logged_in')){
                $session_data          =   $this->session->userdata('logged_in');
                $data['username']      =   $session_data['username'];
                $data['email']         =   $session_data['email'];
                $data['user']          =   $this->user; 

                $data['customer']    	 = 	 $this->Auth_Model->get();

              	$data['content']       =   "Customer/index";
        		$this->load->view('Dashboard/homedashboard',$data);
        }else {
                    redirect('Auth/login', 'refresh');
        }

		
	}

	public function edit(){

		if(@$_POST){
			$this->session->set_userdata('customer_id',$this->input->post('id'));
		}

		if($this->session->userdata('logged_in')){
                $session_data          = $this->session->userdata('logged_in');
                $data['username']      = $session_data['username'];
                $data['email']         = $session_data['email'];
                $data['user']   =   $this->user;  

			$data['data']				=	$this->Auth_Model->edit($this->session->userdata('customer_id'));

			$data['prefix'] 		=   array('Mr','Mrs','Ms','Miss');
      $data['type']       =   array('admin','user');
			$data['province']		= 	$this->province_model->get();
			$data['customer']		= 	$this->Auth_Model->get();
			$data['content']		=	"Customer/edit";
			$this->load->view('Dashboard/homedashboard',$data);
		}else {
                    redirect('Auth/login', 'refresh');
        }
	}



	public function save_edit(){
    
      $sessce =  $this->Auth_Model->save_edit();
      ($sessce==TRUE?$txt_status=1:$txt_status=$this->db->error()['code']);
          echo json_encode(array("txt_status"=>"$txt_status"));
          unset($_POST['id_customer']);
          unset($_POST['name']);

  }

  	public function delete(){

    if(@$_POST){
        $data_session['delete_customer']      =  $this->input->post('id');
      $this->session->set_userdata($data_session);
    }

    $succ = $this->Auth_Model->delete();
    echo ($succ==1?"TRUE":$this->db->error()['code']);
    $this->session->unset_userdata('delete_customer');
  	}

}