<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_controller{

	public function __construct()
    {
        parent::__construct();

        
       	
    }

    public function check_login(){

			if($this->input->post("btn_login")!=null){

				$user					=	$this->input->post('user');
				$pass					=   $this->input->post('pass');

				$result	= $this->Auth_Model->check_login($user,$pass);

				if($result){
			     	foreach($result as $row){
			       		$sess_array = array(
			         			      'id'           =>  $row->id,
			         			      'username'     =>  $row->username,
			         			      'email'	     =>	 $row->email,
			       		);
			       $this->session->set_userdata('logged_in', $sess_array);
			      
			    }
			    $this->user  = $this->Auth_Model->get_type_user()->type_user;
				    $data['use']  =  $this->user;
				    if ($data['use']=='admin') {
				    	redirect('Dashboard', 'refresh');
				    } else {
				    	redirect('Dashboard/intro', 'refresh');
				    }

				}else{					

					$data['login_failed']	=  true;
					$data['user']    = $this->input->post('user');
			     	$this->load->view($this->router->class."/login",$data);
			    }									
			}else{
					$this->load->view($this->router->class."/login",$data);
			}
			
	}

	public function register(){
		$data['users']		    = 	$this->Auth_Model->get();
		$data['prefix'] 		=   array('Mr','Mrs','Ms','Miss');
		$data['province']		= 	$this->province_model->get();
		


		$this->load->view($this->router->class."/register",$data);
	}

	public function login(){
	
	$this->load->view($this->router->class."/login");

	}

	public function add(){
		$data['prefix'] 		=   array('Mr','Mrs','Ms','Miss');
		$data['province']		= 	$this->province_model->get();
		$data['check_province'] =   $this->input->post('province');
		$data['check_prefix']	=	$this->input->post('prefix');
 		
		$this->form_validation->set_rules('prefix', 'คำนำหน้า', 'required');
		$this->form_validation->set_rules('fullname', 'ชื่อ - สกุล',  'required|min_length[5]|max_length[25]|regex_match[/^[a-zA-Z" "]*$/]');
        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
        $this->form_validation->set_rules('username', 'Username', 'required|max_length[21]|regex_match[/^[a-zA-Z0-9]*$/]');
        $this->form_validation->set_rules('pwd', 'Password', 'required|min_length[4]|max_length[8]|regex_match[/^[a-zA-Z0-9]*$/]');
        $this->form_validation->set_rules('ccpwd', 'Re Enter Password', 'required');
        $this->form_validation->set_rules('address', 'ที่อยู่', 'required|max_length[50]|regex_match[/^[a-zA-Z0-9" ""."","]*$/]');
        $this->form_validation->set_rules('province', 'จังหวัด', 'required');
        $this->form_validation->set_rules('postalcode', 'รหัสไปรษณีย์', 'required|regex_match[/^[0-9]{5}$/]');
        $this->form_validation->set_rules('phonenumber', 'เบอร์โทรศัพท์', 'required|regex_match[/^[0-9]{10}$/]');
        $this->form_validation->set_rules('birthdate', 'วันเกิด', 'required');
    
        
        if ($this->form_validation->run() == FALSE)
        {
           
            $data['fullname']    = $this->input->post('fullname');
			$data['email']       = $this->input->post('email');
			$data['username']    = $this->input->post('username');
			$data['address']     = $this->input->post('address');
			$data['postalcode']  = $this->input->post('postalcode');
			$data['phonenumber'] = $this->input->post('phonenumber');
			$data['birthdate']   = $this->input->post('birthdate');
			$data['error']		 = validation_errors();
			
			
			$this->load->view($this->router->class."/register",$data);

        }
        else{
        		$data['double_user']    = $this->Auth_Model->store();
        		if($data['double_user']){
        			$data['fullname']    = $this->input->post('fullname');
					$data['email']       = $this->input->post('email');
					$data['username']    = $this->input->post('username');
					$data['address']     = $this->input->post('address');
					$data['province2']   = $this->input->post('province');
					$data['postalcode']  = $this->input->post('postalcode');
					$data['phonenumber'] = $this->input->post('phonenumber');
					$data['birthdate']   = $this->input->post('birthdate');
        			$this->load->view($this->router->class."/register",$data);
        		}else{
        			$this->load->view($this->router->class."/login");
        		}	
        			
        }	

	}

	
}
