<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
 
  public function __construct(){
        parent::__construct();

        if($this->session->userdata('logged_in') == null){
          redirect('Auth/login', 'refresh');
        }
       $this->user  = $this->Auth_Model->get_type_user()->type_user;
        
  }
  public function index(){
      $data_section = array(
        'id_menu'         =>  "menu_2",
        'submenu_id'      =>  "submenu-2_0",
        'this_section'    =>  "index"
      );
      $this->session->set_userdata($data_section);

              

              if($this->session->userdata('logged_in')){
                $session_data          = $this->session->userdata('logged_in');
                $data['username']      = $session_data['username'];
                $data['email']         = $session_data['email'];
                $data['user']   =   $this->user; 

                $data['category']   =   $this->Category_model->get();

                $data['content']      = "Category/index";
                $this->load->view('Dashboard/homedashboard',$data);
              }else {
                    redirect('Auth/login', 'refresh');
              }

  }

  public function add(){

    $data_section = array(
        'id_menu'         =>  "menu_2",
        'submenu_id'      =>  "submenu-2_1",
        'this_section'    =>  "add"
      );
      $this->session->set_userdata($data_section);


              
      if($this->session->userdata('logged_in')){
                $session_data          = $this->session->userdata('logged_in');
                $data['username']      = $session_data['username'];
                $data['email']         = $session_data['email']; 
                $data['user']   =   $this->user;

                $data['content']    = "Category/add";
                $this->load->view('Dashboard/homedashboard',$data);
      }else {
                redirect('Auth/login', 'refresh');
      }

  }

  public function save_add(){

        if($this->session->userdata('logged_in')){
                $session_data          = $this->session->userdata('logged_in');
                $data['username']      = $session_data['username'];
                $data['email']         = $session_data['email']; 
                $data['user']   =   $this->user;

                $this->form_validation->set_rules('category', 'หัวข้อข่าว', 'required');
         
                if ($this->form_validation->run() == FALSE){
                  $data['error']      = validation_errors();
                  $data['user']   =   $this->user;


                  $data['content']    = "Category/add";
                  $this->load->view('Dashboard/homedashboard',$data);

                  } else {

                    $data['category_save']   =   $this->Category_model->store();
                    $data['user']   =   $this->user;
                    $data['category']   =   $this->Category_model->get();
                    $data['content']    = "Category/index";
                  $this->load->view('Dashboard/homedashboard',$data);
                }
      }else {
                redirect('Auth/login', 'refresh');
      }

  }

  public function edit(){

    if(@$_POST){
      $this->session->set_userdata('category_id',$this->input->post('id'));
    }
    if($this->session->userdata('logged_in')){
                $session_data          = $this->session->userdata('logged_in');
                $data['username']      = $session_data['username'];
                $data['email']         = $session_data['email']; 
                $data['user']   =   $this->user; 

          $data['data']       = $this->Category_model->edit($this->session->userdata('category_id'));
          $data['content']    = "Category/edit";
          $this->load->view('Dashboard/homedashboard',$data);
    }else {
       redirect('Auth/login', 'refresh');
    }
  }

  public function save_edit(){

      

          if($this->session->userdata('logged_in')){
                $session_data          = $this->session->userdata('logged_in');
                $data['username']      = $session_data['username'];
                $data['email']         = $session_data['email']; 
                $data['user']   =   $this->user;

                $this->form_validation->set_rules('category_name', 'หัวข้อข่าว', 'required');
         
                if ($this->form_validation->run() == FALSE){
                  $data['error']      = validation_errors();
                  $data['user']   =   $this->user;


                  $data['content']    = "Category/add";
                  $this->load->view('Dashboard/homedashboard',$data);

                  } else {

                    $sessce =  $this->Category_model->save_edit();
                    ($sessce==TRUE?$txt_status=1:$txt_status=$this->db->error()['code']);
                        echo json_encode(array("txt_status"=>"$txt_status"));
                        unset($_POST['id_category']);
                        unset($_POST['category_name']);
                        redirect('Category', 'refresh');
                }
      }else {
                redirect('Auth/login', 'refresh');
      }

  }

  public function delete(){

      if(@$_POST){
          $data_session['delete_category']      =  $this->input->post('id');
        $this->session->set_userdata($data_session);
      }

      $succ = $this->Category_model->delete();
      echo ($succ==1?"TRUE":$this->db->error()['code']);
      $this->session->unset_userdata('delete_category');
  }


}