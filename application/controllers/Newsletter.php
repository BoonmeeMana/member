<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller {

	public function __construct(){
        parent::__construct();

        if($this->session->userdata('logged_in') == null){
          redirect('Auth/login', 'refresh');
        }
        $this->user  = $this->Auth_Model->get_type_user()->type_user;
        
 	}

	public function index(){

		$data_section = array(
					'id_menu'						=>	"menu_3",
					'submenu_id'					=>	"submenu-3_0",
					'this_section'					=>	"index"
		);
		$this->session->set_userdata($data_section);

		
        if($this->session->userdata('logged_in')){
                $session_data          = $this->session->userdata('logged_in');
                $data['username']      = $session_data['username'];
                $data['email']         = $session_data['email']; 
				$data['user']		= 	$this->user;
                $data['newsletter']	= 	$this->Newsletter_model->get();

              	$data['content']      = "Newsletter/index";
              	$this->load->view('Dashboard/homedashboard',$data);
        }else {
                    redirect('Auth/login', 'refresh');
        }
		
	}

	public function add(){

		$data_section = array(
				'id_menu'			=>	"menu_3",
				'submenu_id'		=>	"submenu-3_1",
				'this_section'		=>	"add"
			);
			$this->session->set_userdata($data_section);

		if($this->session->userdata('logged_in')){
                $session_data          = $this->session->userdata('logged_in');
                $data['username']      = $session_data['username'];
                $data['email']         = $session_data['email']; 
				$data['user']		= 	$this->user;
				
                $data['category']		= 	$this->Category_model->get();
            	$data['content']    = "Newsletter/add";
            	$this->load->view('Dashboard/homedashboard',$data);
        }else {
                    redirect('Auth/login', 'refresh');
        }	

	}

	public function save_add(){
		if($this->session->userdata('logged_in')){
                $session_data          = $this->session->userdata('logged_in');
                $data['username']      = $session_data['username'];
                $data['email']         = $session_data['email']; 
				$data['user']		    = 	$this->user;
				$data['category']		= 	$this->Category_model->get();

				$this->form_validation->set_rules('category', 'หัวข้อข่าว', 'required');
				$this->form_validation->set_rules('subject', 'ชื่อจดหมายข่าว', 'required');

			if ($this->form_validation->run() == FALSE){
				$data['check_category']	=	$this->input->post('category');
				$data['subject']		=	$this->input->post('subject');
				$data['error']		 	=   validation_errors();
				

				$data['content']		=	"Newsletter/add";
			$this->load->view('Dashboard/homedashboard',$data);

			} else {

				$data['newsletter_save']  	= 	$this->Newsletter_model->store();
				$data['newsletter']		= 	$this->Newsletter_model->get();

	            $data['content']      	= 	"Newsletter/index";
			$this->load->view('Dashboard/homedashboard',$data);
			}
	        }else {
	                    redirect('Auth/login', 'refresh');
	        }	
		
	}

	public function edit(){

		if(@$_POST){
			$this->session->set_userdata('newsletter_id',$this->input->post('id'));
		}


		if($this->session->userdata('logged_in')){
                $session_data          = $this->session->userdata('logged_in');
                $data['username']      = $session_data['username'];
                $data['email']         = $session_data['email']; 
                $data['user']   =   $this->user; 
			$data['data']				=	$this->Newsletter_model->edit($this->session->userdata('newsletter_id'));

			$data['category']		= 	$this->Category_model->get();
			$data['newsletter']		= 	$this->Newsletter_model->get();
			$data['content']		=	"Newsletter/edit";
			$this->load->view('Dashboard/homedashboard',$data);
		}else {
                    redirect('Auth/login', 'refresh');
        }	
	}

	public function save_edit(){

				$sessce =  $this->Newsletter_model->save_edit();
	     		($sessce==TRUE?$txt_status=1:$txt_status=$this->db->error()['code']);

	      		$this->session->set_userdata('txt_status','true');
				unset($_POST['id_newsletter']);
	          	unset($_POST['newsletter_name']);
	          	redirect('Newsletter', 'refresh');
			
	}

	public function delete(){

		if(@$_POST){
		    $data_session['delete_newsletter']    =   $this->input->post('id');
			$this->session->set_userdata($data_session);
		}

		$succ = $this->Newsletter_model->delete();
		echo ($succ==1?"TRUE":$this->db->error()['code']);
		$this->session->unset_userdata('delete_newsletter');
	}

	public function popup_detail($id){
		 $data['newsletter']	    = 	$this->Newsletter_model->get_popup($id);
		 $this->load->view('Newsletter/popup_content',$data);
	}
}