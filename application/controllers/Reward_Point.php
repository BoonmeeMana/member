<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reward_Point extends CI_Controller {

	public function __construct(){
        parent::__construct();

        if($this->session->userdata('logged_in') == null){
          redirect('Auth/login', 'refresh');
        }
        $this->user  = $this->Auth_Model->get_type_user()->type_user;
        
 	}

	public function index(){

		$data_section = array(
          'id_menu'           =>  "menu_8",
          'submenu_id'          =>  "submenu-8_0",
          'this_section'          =>  "index"
    );
    $this->session->set_userdata($data_section);

    
        if($this->session->userdata('logged_in')){
                $session_data          =  $this->session->userdata('logged_in');
                $data['username']      =  $session_data['username'];
                $data['email']         =  $session_data['email']; 
                $data['user']          =  $this->user;
                $data['data_user']     =  $this->Auth_Model->get_data_user();
                $data['data_point']    =  $this->Point_model->get_point();

                $data['content']      = "Reward_Point/index";
                $this->load->view('Dashboard/homedashboard',$data);
        }else {
                    redirect('Auth/login', 'refresh');
        }
		
	}

  public function history(){

    $data_section = array(
          'id_menu'           =>  "menu_8",
          'submenu_id'          =>  "submenu-8_1",
          'this_section'          =>  "index"
    );
    $this->session->set_userdata($data_section);

    
        if($this->session->userdata('logged_in')){
                $session_data          =  $this->session->userdata('logged_in');
                $data['username']      =  $session_data['username'];
                $data['email']         =  $session_data['email']; 
                $data['user']          =  $this->user;
                $data['data_user']     =  $this->Auth_Model->get_data_user();
                $data['data_point']    =  $this->Point_model->get_point();

                $data['content']      = "Reward_Point/history";
                $this->load->view('Dashboard/homedashboard',$data);
        }else {
                    redirect('Auth/login', 'refresh');
        }
  }

  public function approve(){

    $data_section = array(
          'id_menu'           =>  "menu_9",
          'submenu_id'          =>  "submenu-9_1",
          'this_section'          =>  "index"
    );
    $this->session->set_userdata($data_section);

    
        if($this->session->userdata('logged_in')){
                $session_data          =  $this->session->userdata('logged_in');
                $data['username']      =  $session_data['username'];
                $data['email']         =  $session_data['email']; 
                $data['user']          =  $this->user;
                /*$data['data_user']     =  $this->Auth_Model->get_data_user();
                print_r($data['data_user']);
                exit;*/
                $data['data_point']    =  $this->Point_model->get_point();



                $data['content']      = "Reward_Point/approve";
                $this->load->view('Dashboard/homedashboard',$data);
        }else {
                    redirect('Auth/login', 'refresh');
        }
  }

  public function show_point(){

    $data_section = array(
          'id_menu'           =>  "menu_9",
          'submenu_id'          =>  "submenu-9_0",
          'this_section'          =>  "index"
    );
    $this->session->set_userdata($data_section);

    
        if($this->session->userdata('logged_in')){
                $session_data          =  $this->session->userdata('logged_in');
                $data['username']      =  $session_data['username'];
                $data['email']         =  $session_data['email']; 
                $data['user']          =  $this->user;
                $data['data_user']     =  $this->Auth_Model->get_data_user();
                $data['data_point']    =  $this->Point_model->get_point();

                $data['content']      = "Reward_Point/show_point";
                $this->load->view('Dashboard/homedashboard',$data);
        }else {
                    redirect('Auth/login', 'refresh');
        }
  }

  public function delete(){

    if(@$_POST){
        $data_session['delete_point']    =   $this->input->post('id');
      $this->session->set_userdata($data_session);
    }

    $succ = $this->Point_model->delete();
    echo ($succ==1?"TRUE":$this->db->error()['code']);
    $this->session->unset_userdata('delete_point');
  }

}