<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_controller{

	public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('logged_in') == null){
        	redirect('Auth/login', 'refresh');
        }
         $this->user  = $this->Auth_Model->get_type_user()->type_user;
   		
        
    }

    public function index(){
    	
    	$data_section = array(
        'id_menu'         =>  "menu_1",
        'this_section'    =>  "index"
      );
      $this->session->set_userdata($data_section);

      if(@$_POST){
			$this->session->set_userdata('type_user',$this->input->post('type_user'));
		}
       

		if($this->session->userdata('logged_in')){
        	$session_data            =  $this->session->userdata('logged_in');
			$data['username']        =  $session_data['username'];
			$data['email']           =  $session_data['email'];
			$data['user']		     =  $this->user;

			$data['content']	     =	"Dashboard/home_newsletter";
        	$this->load->view($this->router->class.'/homedashboard',$data);
        }else {
        	redirect('Auth/login', 'refresh');
        }

	}

	public function intro(){
    	
    	$data_section = array(
        'id_menu'         =>  "menu_6",
        'this_section'    =>  "index"
      );
      $this->session->set_userdata($data_section);

      if(@$_POST){
			$this->session->set_userdata('type_user',$this->input->post('type_user'));
		}
       

		if($this->session->userdata('logged_in')){
        	$session_data          =  $this->session->userdata('logged_in');
			$data['username']      =  $session_data['username'];
			$data['email']         =  $session_data['email'];
			$data['user']		   =  $this->user;

			
				$data['content']	   =	"Dashboard/newsletter";
			
		
        	$this->load->view($this->router->class.'/homedashboard',$data);
        }else {
        	redirect('Auth/login', 'refresh');
        }

	}

	public function newsletter(){
		$data['category']    = $this->Category_model->store();

	   	$data['content']		=	"Newsletter/add";
		$this->load->view('Dashboard/homedashboard',$data);
	
	}

	

	public function logout(){
	   
	   $this->session->unset_userdata('logged_in');
	   session_destroy();
	   redirect('Auth/login', 'refresh');
	
	}
	

}