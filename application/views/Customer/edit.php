
<script type="text/javascript" src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">
  <style type="text/css" media="all">
    .chosen-rtl .chosen-drop { left: -9000px; }
  </style>
<script type="text/javascript">
  $( document ).ready(function() {

      $('#save_edit').submit(function(){
            $.post('<?=base_url().$this->router->class.'/save_edit'?>',$("form").serialize(),function( data ){

                  if(data.txt_status == true){
                      swal("บันทึกการแก้ไขสำเร็จ!", "กรุณาคลิกที่ปุ่ม!", "success");
                      setTimeout("redirect_page();",1000); 
                  }else if(data.txt_status == 0){
                      swal("Data is Double" , "error");
                  }else{
                      swal("Error "+data.txt_status,"Database Error "+data.txt_status, "error");
                  }

                  if(data.validation!=null){
                    
                    swal(data.validation, "error");
  
                  }

            },"json");
             
            return false;  
      });
        
  });

function redirect_page(){
    window.location = "<?=base_url().$this->router->class?>/index";
}
</script>

<section class="content-header">
          <h1>ข้อมูลสมาชิก</h1>
          <ol class="breadcrumb">
            <li><a href="#"> Home</a></li>
            <li><a href="<?=base_url()?>Customer/index"> Customer</a></li>
            <li class="active">View Member</li>
          </ol>
    </section>

<section class="content">
    <div class="row">
        <section class="col-lg-12">  
            <div class="box box-info">
              <div class="box-header with-border">
                <h2 class="box-title">แบบฟอร์มแก้ไขข้อมูลสมาชิก</h2>
              </div><!-- /.box-header -->
<?php

?>
             <!-- form start -->
              <form class="form-horizontal" method="post" id="save_edit">
                <div class="box-body">
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="prefix">Prefix:</label>
                      <div class="col-sm-2">
                        <select class="form-control" name="prefix">
                          <option value="">คำนำหน้า</option>
                            <?php
                              foreach ($prefix as $key => $value) {
                            ?>
                          <option <?=$value==@$data['prefix']? 'selected':''  ?> value="<?=$value; ?>">
                              <?php echo $value; ?>
                          </option> 
                              
                            <?php
                              }
                          ?>
                      </select>
                      </div>
                    <label class="control-label col-sm-3" for="fullname">ชื่อ - สกุล:</label>
                    <div class="col-sm-3">
                      <input type="text" class="form-control" name="fullname" placeholder="Enter fullname" value="<?=$data['name']?>">
                    </div>
                  </div>
          
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="address">ที่อยู่:</label>
                    <div class="col-sm-3">
                      <textarea  rows="3" cols="38" name="address"><?=$data['address']?></textarea>
                    </div>

                    <label class="control-label col-sm-2" for="email">E-mail:</label>
                    <div class="col-sm-3">
                      <input type="email" class="form-control" name="email" placeholder="Enter email" value="<?=$data['email']; ?>">
                    </div>
                    <!--<label class="control-label col-sm-1" for="gender">Gender:</label>
                    <div class="col-sm-3">
                      <label class="radio-inline">
                      <input type="radio" name="gender" value="M" <?//php echo ($data['gender']=='M')?'checked':'' ?>size="17">Male
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="gender" value="F" <?//php echo ($data['gender']=='F')?'checked':'' ?>size="17">Female
                    </label>
                    </div>-->
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="Province">จังหวัด:</label>
                      <div class="col-sm-3">
                        <select style="width:250px;" class="chosen-select-deselect" tabindex="12" name="province">
                          <option value="">กรุณาเลือกจังหวัด</option>
                            <?php
                              foreach ($province as $key => $value) {
                            ?>
                              <option <?=$data['province_id']==@$value['id']? 'selected':''  ?> value="<?=$value['id']; ?>">
                              <?php echo $value['province']; ?>
                          </option>

                           
                            <?php
                              }
                          ?>
                      </select>
                    </div>
                  
                    <label class="control-label col-sm-2" for="postalcode">รหัสไปรษณีย์:</label>
                    <div class="col-sm-2">
                      <input type="text" class="form-control" style="width:143px;" name="postalcode" placeholder="Enter postalcode" value="<?=$data['postalcode']?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="phonenumber">เบอร์โทรศัพท์:</label>
                    <div class="col-sm-3">
                      <input type="tel" class="form-control" style="width:250px;" name="phonenumber" placeholder="Enter phone number" maxlength="14" value="<?=$data['phonenumber']?>">
                      ตัวอย่าง 0987654321
                    </div>
                  
                    <label class="control-label col-sm-2" for="birthdate">วันเกิด:</label>
                    <div class="col-sm-4">
                      <input id="birthdate" type="date" name="birthdate" onchange="calAge(this);" value="<?=$data['birthdate']?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="Province">กำหนดสิทธิ์:</label>
                      <div class="col-sm-3">
                        <select style="width:250px;" class="form-control" tabindex="12" name="type">
                          <option value=""></option>
                            <?php
                              foreach ($type as $key => $value) {
                            ?>
                              <option <?=$value==@$data['type_user']? 'selected':''  ?> value="<?=$value; ?>">
                                  <?php echo $value; ?>
                              </option>

                           
                            <?php
                              }
                          ?>
                      </select>
                    </div>
                  </div>

                  <script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>
                  <script type="text/javascript">
                      var config = {
                       
                        '.chosen-select-deselect'  : {allow_single_deselect:true},
                     
                      }
                      for (var selector in config) {
                        $(selector).chosen(config[selector]);
                      }
                  </script>
                </div>


                  <input id="id"  type="hidden" class="form-control"   name="id_customer" value="<?=$data['id']?>" >
                  <div class="box-footer clearfix">
                    <button type="submit" class="col-sm-2 pull-right btn btn-primary" id="edit">บันทึกการแก้ไข  <i class="fa fa-arrow-circle-right"></i></button>
                  </div>

              </form>
            </div><!-- /.box -->

        </section>
    </div>
</section>

