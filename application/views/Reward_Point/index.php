<script type="text/javascript" src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">

<link rel="stylesheet" href="<?=base_url();?>publicts/chosen/chosen.css">


<section class="content-header">
          <h1>คะแนนสะสม</h1>
          <ol class="breadcrumb">
            <li><a href="#"> Home</a></li>
            <li><a href="<?=base_url()?>User_Account/index"> Customer</a></li>
            <li class="active">คะแนนสะสม</li>
          </ol>
    </section>

<section class="content">
    <div class="row">
        <section class="col-lg-12">  
            <div class="box box-info">
                <div class="box-header with-border">
                    <!-- <h3>คะแนนสะสม</h3> -->
                    <p>คะแนนสะสมของคุณ <?=$data_user['name'] ?> </p>
                    <?php
                       
                    ?>
                    <table id="example22" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th  style="text-align: center; background-color:#C1CDCD;width:1px;">No.</th>
                                <th class= "code" style="text-align: center;background-color:#C1CDCD;width:90px;">เรื่อง</th>
                                <th class= "code" style="text-align: center;background-color:#C1CDCD;width:60px;">ประเภท</th>
                                <th class= "code" style="text-align: center;background-color:#C1CDCD;width:30px;">คะแนน</th>
                                </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i=1;
                                foreach (@$data_point as $key => $value) {
                                    if ($data_user['name']==$value['name']) {
                            ?>
                                    <tr>
                                        <td style="text-align: center;"><?=$i++?></td>
                                        <td style="text-align: center;"><?=$value['rewardpoint_name']; ?></td>
                                        <td style="text-align: center;"><?=$value['rewardpoint_category']; ?></td>
                                        <td style="text-align: center;"><?=$value['rewardpoint_score']; ?></td>
                                    </tr>
                                    <?php

                                           /* $a=$value['rewardpoint_score'];*/
                                            
                                    }
                                }
                            ?>
                        </tbody>
                        <tfoot>
                                    <tr>
                                        <td style="text-align: center;"></td>
                                        <td style="text-align: center;"></td>
                                        <td style="text-align: right;">คะแนนสะสมที่มี</td>
                                        <td style="text-align: center;">5
                                            <?php
                                               /* echo array_sum($a);*/ 
                                            ?>
                                        </td>
                                    </tr>
                        </tfoot>
                            
                        
                    </table>

                </div>

            </div>
        </section>
    </div>
</section>

<script src="<?=base_url();?>/publicts/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="<?=base_url()?>/publicts/dashboard/plugins/resources/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
    
    $(document).ready(function() {
    $('#example22').DataTable();
    });
</script>
