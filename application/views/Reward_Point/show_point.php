<script type="text/javascript" src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">

<link rel="stylesheet" href="<?=base_url();?>publicts/chosen/chosen.css">

<script type="text/javascript">
/*function delete_point(id){

      swal({  title:  "คุณแน่ใจใช่มั้ย?",   
              text:   "คุณจะไม่สามารถกู้คืนไฟล์นี้!", 
                
              type:   "warning",   
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55", 
              closeOnConfirm: false 
            }, 
          
        function(){           
          $.post("<?=base_url().$this->router->class.'/delete'?>",{'id':id}).done(
                function(data){
                  if(data=="TRUE"){
                    swal("ลบสำเร็จ");               
                    setTimeout("redirect();",1000);                   
                  }else{
                        sweetAlert("error");
                  }
                }
          );

        });

  }

  function redirect(){
      window.location = "<?=base_url().$this->router->class?>";
  }*/
</script>

<section class="content-header">
          <h1>คะแนนสะสม</h1>
          <ol class="breadcrumb">
            <li><a href="#"> Home</a></li>
            <li><a href="<?=base_url()?>User_Account/index"> Customer</a></li>
            <li class="active">Reward Point</li>
          </ol>
    </section>

<section class="content">
    <div class="row">
        <section class="col-lg-12">  
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3>ตารางแสดงคะแนนสะสม</h3>
                    <br>
                    <?php
                       //print_r($data_point);
                    ?>
                    <table id="example22" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th  style="text-align: center; background-color:#C1CDCD;width:1px;">No.</th>
                                <th class= "code" style="text-align: center;background-color:#C1CDCD;width:70px;">ชื่อสมาชิก</th>
                                <th class= "code" style="text-align: center;background-color:#C1CDCD;width:60px;">คะแนนสะสม</th>
                                <th class= "code" style="text-align: center;background-color:#C1CDCD;width:30px;">หมายเหตุ</th><!-- 
                                <th class= "edit no-sort" style='text-align: center; background-color:#FFFACD;width:30px;'>อนุมัติ</th> -->
                                <!-- <th class= "delete no-sort" style='text-align: center; background-color:#FF9999;width:30px;'>ลบ</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i=1;
                                foreach (@$data_point as $key => $value) {
                            ?>
                            <tr>
                                <td style="text-align: center;"><?=$i++?></td>
                                <td style="text-align: center;"><?=$value['name']; ?></td>
                                <td style="text-align: center;"><?=$value['rewardpoint_score']; ?></td>
                                <td style="text-align: center;"></td><!-- 
                                <td style="text-align: center;"><a href="javascript:edit(<?=$value['id']?>);" ><font face="Times New Roman" size="3">แก้ไข</font></a></td> -->
                                <!-- <td style="text-align: center;" onclick=" delete_point(<?=$value['id']?>)"><a  href="#" ><font face="Times New Roman" size="3">ลบ</font></a></td> -->
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>

                </div>

            </div>
        </section>
    </div>
</section>

<script src="<?=base_url();?>/publicts/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="<?=base_url()?>/publicts/dashboard/plugins/resources/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
    
    $(document).ready(function() {
    $('#example22').DataTable();
    });
</script>