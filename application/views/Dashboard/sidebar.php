
<section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url();?>/publicts/dashboard/img/userprofile.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?=@$username?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> ออนไลน์</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>

          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            


            <li class="treeview  <?php echo $this->session->userdata('id_menu') == 'menu_1' ? 'active' : '' ?>" id="menu_1" >
              <a href="<?=base_url()?>Dashboard"  class="click_menu" data-id_menu="menu_2" data-main_menu="Dashboard">

                  <span>หน้าแรก</span> <i class="fa fa-angle-left pull-right"></i>
                  
              </a>
            </li>


            <li class="treeview  <?php echo $this->session->userdata('id_menu') == 'menu_2' ? 'active' : '' ?>" id="menu_2" >
              <a href="#"  class="click_menu" data-id_menu="menu_2" data-main_menu="Newsletter category">

                  <span>หัวข้อจดหมายข่าว</span> <i class="fa fa-angle-left pull-right"></i>
                  
              </a>
              <ul class="treeview-menu">
                <li id="submenu-2_0" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-2_0' ? 'active' : '' ?>" ><a href="<?=base_url()?>Category/index"><i class="fa fa-circle-o"></i> แสดงหัวข้อข่าว</a></li>
                <li id="submenu-2_1" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-2_1' ? 'active' : '' ?>" ><a href="<?=base_url()?>Category/add"><i class="fa fa-circle-o"></i> เพิ่มหัวข้อข่าว</a></li>
              </ul>
            </li>


            <li class="treeview  <?php echo $this->session->userdata('id_menu') == 'menu_3' ? 'active' : '' ?>" id="menu_3" >
              <a href="#"  class="click_menu" data-id_menu="menu_3" data-main_menu="Newsletter group">
                
                 <span>จดหมายข่าว</span> <i class="fa fa-angle-left pull-right"></i>
                
              </a>
              <ul class="treeview-menu">
                <li id="submenu-3_0" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-3_0' ? 'active' : '' ?>" ><a href="<?=base_url()?>Newsletter/index"><i class="fa fa-circle-o"></i> แสดงจดหมายข่าว</a></li>
                <li id="submenu-3_1" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-3_1' ? 'active' : '' ?>" ><a href="<?=base_url()?>Newsletter/add"><i class="fa fa-circle-o"></i> เพิ่มจดหมายข่าว</a></li>
              </ul>
            </li>

            <li class="treeview  <?php echo $this->session->userdata('id_menu') == 'menu_7' ? 'active' : '' ?>" id="menu_7" >
              <a href="#"  class="click_menu" data-id_menu="menu_7" data-main_menu="Customer">
                
                 <span>ส่ง E-mail</span> <i class="fa fa-angle-left pull-right"></i>
                
              </a>
              <ul class="treeview-menu">
                <li id="submenu-7_0" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-7_0' ? 'active' : '' ?>" ><a href="<?=base_url()?>Send/sending"><i class="fa fa-circle-o"></i> แสดงรายการส่ง</a></li>
                <li id="submenu-7_1" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-7_1' ? 'active' : '' ?>" ><a href="<?=base_url()?>Send/Send_Mail"><i class="fa fa-circle-o"></i> เพิ่มรายการส่ง</a></li>
              
              </ul>
            </li>


            <li class="treeview  <?php echo $this->session->userdata('id_menu') == 'menu_4' ? 'active' : '' ?>" id="menu_4" >
              <a href="#"  class="click_menu" data-id_menu="menu_4" data-main_menu="Customer">
                
                 <span>ข้อมูลสมาชิก</span> <i class="fa fa-angle-left pull-right"></i>
                
              </a>
              <ul class="treeview-menu">
                <li id="submenu-4_0" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-4_0' ? 'active' : '' ?>" ><a href="<?=base_url()?>Customer/index"><i class="fa fa-circle-o"></i> แสดงข้อมูลสมาชิก</a></li>
              </ul>
            </li>

          <li class="treeview  <?php echo $this->session->userdata('id_menu') == 'menu_9' ? 'active' : '' ?>" id="menu_9" >
              <a href="#"  class="click_menu" data-id_menu="menu_9" data-main_menu="Newsletter group">
                
                 <span>คะแนนสะสม</span> <i class="fa fa-angle-left pull-right"></i>
                
              </a>
              <ul class="treeview-menu">
                <li id="submenu-9_0" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-9_0' ? 'active' : '' ?>" ><a href="<?=base_url()?>Reward_Point/show_point"><i class="fa fa-circle-o"></i> แสดงตารางคะแนน</a></li>
                <!-- <li id="submenu-9_1" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-9_1' ? 'active' : '' ?>" ><a href="<?=base_url()?>Reward_Point/approve"><i class="fa fa-circle-o"></i> Approveคะแนน</a></li> -->
              </ul>
            </li>  

            
            <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-share"></i> <span>Multilevel</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li>
                      <a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
              </ul>
            </li> -->
            
          </ul>
        </section>