
<section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url();?>/publicts/dashboard/img/userprofile.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?=@$username?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> ออนไลน์</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Menu for Customer</li>
            


            <li class="treeview  <?php echo $this->session->userdata('id_menu') == 'menu_6' ? 'active' : '' ?>" id="menu_6" >
              <a href="<?=base_url()?>Dashboard/intro"  class="click_menu" data-id_menu="menu_6" data-main_menu="Dashboard">

                  <span>หน้าแรก</span> <i class="fa fa-angle-left pull-right"></i>
                  
              </a>
            </li>


            <li class="treeview  <?php echo $this->session->userdata('id_menu') == 'menu_5' ? 'active' : '' ?>" id="menu_5" >
              <a href="#"  class="click_menu" data-id_menu="menu_4" data-main_menu="Customer">
                
                 <span>บัญชีผู้ใช้</span> <i class="fa fa-angle-left pull-right"></i>
                
              </a>
              <ul class="treeview-menu">
                <li id="submenu-5_3" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-5_3' ? 'active' : '' ?>" ><a href="<?=base_url()?>User_Account/index"><i class="fa fa-circle-o"></i> ข้อมูลบัญชีผู้ใช้</a></li>
                <li id="submenu-5_0" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-5_0' ? 'active' : '' ?>" ><a href="<?=base_url()?>User_Account/accountdata"><i class="fa fa-circle-o"></i> แก้ไขข้อมูลบัญชี</a></li>
                <li id="submenu-5_1" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-5_1' ? 'active' : '' ?>" ><a href="<?=base_url()?>User_Account/contactdata"><i class="fa fa-circle-o"></i> แก้ไขข้อมูลติดต่อ</a></li>
              </ul>
            </li>

            <li class="treeview  <?php echo $this->session->userdata('id_menu') == 'menu_10' ? 'active' : '' ?>" id="menu_10" >
              <a href="#"  class="click_menu" data-id_menu="menu_10" data-main_menu="Customer">
                
                 <span>จดหมายข่าว</span> <i class="fa fa-angle-left pull-right"></i>
                
              </a>
              <ul class="treeview-menu">
                <li id="submenu-10_0" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-10_0' ? 'active' : '' ?>" ><a href="<?=base_url()?>User_Account/follow_newsletter"><i class="fa fa-circle-o"></i> ติดตามจดหมายข่าว</a></li>
                </ul>
            </li>

            <li class="treeview  <?php echo $this->session->userdata('id_menu') == 'menu_8' ? 'active' : '' ?>" id="menu_8" >
              <a href="#"  class="click_menu" data-id_menu="menu_8" data-main_menu="Customer">
                
                 <span>คะแนนสะสม</span> <i class="fa fa-angle-left pull-right"></i>
                
              </a>
              <ul class="treeview-menu">
                <li id="submenu-8_0" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-8_0' ? 'active' : '' ?>" ><a href="<?=base_url()?>Reward_Point/index"><i class="fa fa-circle-o"></i> คะแนนสะสมทั้งหมด</a></li>
                <li id="submenu-8_1" class="<?php echo $this->session->userdata('submenu_id') == 'submenu-8_1' ? 'active' : '' ?>" ><a href="<?=base_url()?>Reward_Point/history"><i class="fa fa-circle-o"></i> ประวัติการใช้</a></li>
               </ul>
            </li>
            
            
            
            
            
            
            <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-share"></i> <span>Multilevel</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li>
                      <a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
              </ul>
            </li> -->
            
          </ul>
        </section>