
<script type="text/javascript" src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">
  <style type="text/css" media="all">
    .chosen-rtl .chosen-drop { left: -9000px; }
  </style>
<script type="text/javascript">
  $( document ).ready(function() {

      $('#save_edit').submit(function(){
            $.post('<?=base_url().$this->router->class.'/save_edit'?>',$("form").serialize(),function( data ){

                  if(data.txt_status == true){
                      swal("บันทึกสำเร็จ!", "", "success");
                      setTimeout("redirect_page();",1000); 
                  }else if(data.txt_status == 0){
                      swal("Data is Double" , "error");
                  }else{
                      swal("Error "+data.txt_status,"Database Error "+data.txt_status, "error");
                  }

                  if(data.validation!=null){
                    
                    swal(data.validation, "error");
  
                  }

            },"json");
             
            return false;  
      });
        
  });

function redirect_page(){
    window.location = "<?=base_url().$this->router->class?>/index";
}
</script>

    <section class="content-header">
          <h1>หัวข้อจดหมายข่าว</h1>
          <ol class="breadcrumb">
            <li><a href="#"> Home</a></li>
            <li><a href="<?=base_url()?>Newsletter/index"> Newsletter category</a></li>
            <li class="active">View Newsletter category</li>
          </ol>
    </section>

<section class="content">
    <div class="row">
        <section class="col-lg-12">  
            <div class="box box-info">
              <div class="box-header with-border">
                <h2 class="box-title">แบบฟอร์มแก้ไขหัวข้อจดหมายข่าว</h2>
              </div><!-- /.box-header -->

             <!-- form start -->
             <?php
                if (@$error==true) {
               ?>
                  <div class="alert alert-danger " style="width:750px;">
                      <?=@$error?>
                </div>
              <?php
                }
              ?>
              <form class="form-horizontal" method="post" id="save_edit">
                <div class="box-body">
                  <div class="form-group">
                    <label class="control-label col-sm-3" class="control-label col-sm-1" for="category_name">หัวข้อข่าว :</label>
                    <div class="col-sm-9">
                      <input id="category_name" type="text" class="form-control"   name="category_name" value="<?=$data['category_name']?>" required>
                    </div>
                  </div>
                  
                </div>
                  <input id="category_name"  type="hidden" class="form-control"   name="id_category" value="<?=$data['id']?>" >
                  <div class="box-footer clearfix">
                    <button type="submit" class="col-sm-3 pull-right btn btn-primary" id="edit">บันทึกการแก้ไข <i class="fa fa-arrow-circle-right"></i></button>
                  </div>

              </form>
            </div><!-- /.box -->

        </section>
    </div>
</section>

