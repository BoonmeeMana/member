        
<link rel="stylesheet" href="<?=base_url();?>/publicts/chosen/chosen.css">
<script type="text/javascript" src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">
<script type="text/javascript">

    function delete_category(id){

      swal({  title:  "คุณแน่ใจใช่มั้ย?",   
              text:   "คุณจะไม่สามารถกู้คืนไฟล์นี้ได้!", 
                
              type:   "warning",   
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55", 
              closeOnConfirm: false 
            }, 
          
        function(){           
          $.post("<?=base_url().$this->router->class.'/delete'?>",{'id':id}).done(
                function(data){
                  if(data=="TRUE"){
                    swal("ข้อมูลนี้ถูกลบแล้ว");               
                    setTimeout("redirect();",1000);                   
                  }else{
                        sweetAlert("error");
                  }
                }
          );

        });

  }

  function redirect(){
      window.location = "<?=base_url().$this->router->class?>";
  }
     
    function edit(id){
        $.post("<?=base_url().$this->router->class.'/edit'?>",{'id':id}).done(function(){
            window.location = "<?=base_url().$this->router->class.'/edit'?>";
        }); 
    }

</script>

  <style type="text/css" media="all">

    .chosen-rtl .chosen-drop { left: -9000px; }
  </style>

    <section class="content-header">
          <h1>หัวข้อจดหมายข่าว</h1>
          <ol class="breadcrumb">
            <li><a href="#"> Home</a></li>
            <li><a href="<?=base_url()?>Category/index"> Newsletter category</a></li>
            <li class="active">View Newsletter category</li>
          </ol>
    </section>

<section class="content">
    <div class="row">
        <section class="col-lg-12">  
            <div class="box box-info">
                <div class="box-header with-border">
                    <h2 class="box-title">แสดงหัวข้อจดหมายข่าว</h2>
                    <br>
                    <table id="example21" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th  style="text-align: center; background-color:#C1CDCD;width:1px;">No.</th>
                                <th class= "code" style="text-align: center;background-color:#C1CDCD;width:150px;">หัวข้อข่าว</th>
                                <th class= "edit no-sort" style='text-align: center; background-color:#FFFACD;width:10px;'>แก้ไข</th>
                                <th class= "delete no-sort" style='text-align: center; background-color:#FF9999;width:10px;'>ลบข้อมูล</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i=1;
                                foreach (@$category as $key => $value) {
                            ?>
                            <tr>
                                <td style="text-align: center;"><?=$i++?></td>
                                <td style="text-align: center;"><?=$value['category_name']; ?></td>
                                <td style="text-align: center;"><a  href="javascript:edit(<?=$value['id']?>);" ><font face="TH SARABUN NEW REGULAR" size="3">แก้ไข</font></a></td>
                                <td style="text-align: center;" onclick="delete_category(<?=$value['id']?>)"><a  href="#" ><font face="Times New Roman" size="3">ลบ</font></a></td>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                    
            </div>
           
        </section>
    </div>
</section>

<script src="<?=base_url();?>/publicts/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="<?=base_url()?>/publicts/dashboard/plugins/resources/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function() {
    
        $('#example21').DataTable();
    
    });
</script>
