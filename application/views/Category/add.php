

<link rel="stylesheet" href="<?=base_url();?>/publicts/chosen/chosen.css">
  <style type="text/css" media="all">

    .chosen-rtl .chosen-drop { left: -9000px; }
  </style>

    <section class="content-header">
          <h1>หัวข้อจดหมายข่าว</h1>
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="<?=base_url()?>Dashboard"> Dashboard</a></li>
            <li class="active">Add Category Newsletter</li>
          </ol>
    </section>

<section class="content">
    <div class="row">
        <section class="col-lg-12">  
            <div class="box box-info">
              <div class="box-header with-border">
                <h2 class="box-title">แบบฟอร์มเพิ่มหัวข้อจดหมายข่าว</h2>
              </div><!-- /.box-header -->
              <!-- form start -->

              <form class="form-horizontal"  method="POST" action="<?=base_url()?>Category/save_add">
                <div class="box-body">
                  <?php
                    if (@$error==true) {
                   ?>
                      <div class="alert alert-danger " style="width:750px;">
                          <?=@$error?>
                    </div>
                  <?php
                    }
                  ?>
                  <div class="form-group">
                    <label class="control-label col-sm-3" class="control-label col-sm-1" for="category">หัวข้อจดหมายข่าว :</label>
                    <div class="col-sm-6">
                      <input id="category" type="text" class="form-control"   name="category" onfocus="this.select()" placeholder="<?=$this->lang->line('please');?>" >
                    </div>
                  </div>
                  

                  
                <div class="box-footer clearfix">
                  <button type="submit" name="btnadd" class="col-lg-2 pull-right btn btn-primary" id="add_category">บันทึก <i class="fa fa-arrow-circle-right"></i></button>
                </div>


            <script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>
            <script type="text/javascript">
              var config = {
               
                '.chosen-select-deselect'  : {allow_single_deselect:true},
             
              }
              for (var selector in config) {
                $(selector).chosen(config[selector]);
              }
          </script>
                  
              </form>
            </div><!-- /.box -->

        </section>
    </div>
</section>

