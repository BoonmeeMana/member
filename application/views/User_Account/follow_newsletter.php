

<script type="text/javascript" src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">

<link rel="stylesheet" href="<?=base_url();?>publicts/chosen/chosen.css">
<style type="text/css" media="all">
    .chosen-rtl .chosen-drop { left: -9000px; }
  </style>

<section class="content-header">
          <h1>ส่วนการติดตามจดหมายข่าว</h1>
          <ol class="breadcrumb">
            <li><a href="#"> Home</a></li>
            <li><a href="<?=base_url()?>User_Account/index"> Customer</a></li>
            <li class="active">Follow Newsletter</li>
          </ol>
    </section>

<section class="content">
    <div class="row">
        <section class="col-lg-12">  
            <div class="box box-info">
                <div class="box-header with-border">
                    <br>
                    <h3>เลือกประเภทหัวข้อข่าวที่ต้องการติดตาม</h3>
                    <div class="form-group">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form class="form-horizontal" method="post" action="<?=base_url()?>User_Account/save_follownews">
                                        <?php
                                            foreach (@$category as $key => $value) {
                                        ?>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="follownews[]"
                                                    <?php
                                                        foreach (@$show_follownews as $key => $value2){
                                                            echo $value2['follownews_name']==$value['category_name']?'checked':'';
                                                        }
                                                    ?>
                                            value="<?=$value['category_name']; ?>">
                                            <?=$value['category_name']; ?>
                                        </label><br>
                                        
                                        <?php
                                            }
                                        ?>
                                        <input id="id"  type="hidden" class="form-control"   name="id_customer" value="<?=$data_user['id']?>" >
                                        <button type="submit" class="col-sm-2 pull-right btn btn-primary" id="submit">บันทึก <i class="fa fa-arrow-circle-right"></i></button>
                                        <button type="reset" class="col-sm-2 pull-right btn btn-primary" id="reset">ยกเลิก </button>
                                    </form>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    

                </div><!-- /.box-header -->
                
                    <div class="box-footer clearfix">

                        
                    </div>

            </div>
        </section>
    </div>
</section>

<script src="<?=base_url();?>/publicts/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="<?=base_url()?>/publicts/dashboard/plugins/resources/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
    
    $(document).ready(function() {
    $('#example22').DataTable();
    });
</script>