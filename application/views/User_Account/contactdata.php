

<script type="text/javascript" src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">

<link rel="stylesheet" href="<?=base_url();?>publicts/chosen/chosen.css">

<style type="text/css" media="all">
    .chosen-rtl .chosen-drop { left: -9000px; }
  </style>
<script type="text/javascript">
  $( document ).ready(function() {

      $('#save_edit').submit(function(){
            $.post('<?=base_url().$this->router->class.'/save_edit_contact'?>',$("form").serialize(),function( data ){

                  if(data.txt_status == true){
                      swal("บันทึกการแก้ไขสำเร็จ!", "กรุณาคลิกที่ปุ่ม!", "success");
                      setTimeout("redirect_page();",1000); 
                  }else if(data.txt_status == 0){
                      swal("Data is Double" , "error");
                  }else{
                      swal("Error "+data.txt_status,"Database Error "+data.txt_status, "error");
                  }

                  if(data.validation!=null){
                    
                    swal(data.validation, "error");
  
                  }

            },"json");
             
            return false;  
      });
        
  });

function redirect_page(){
    window.location = "<?=base_url().$this->router->class?>/index";
}
</script>

<section class="content-header">
          <h1>ส่วนควบคุมบัญชีผู้ใช้</h1>
          <ol class="breadcrumb">
            <li><a href="#"> Home</a></li>
            <li><a href="<?=base_url()?>User_Account/index"> Customer</a></li>
            <li class="active">Contact Data</li>
          </ol>
    </section>

<section class="content">
    <div class="row">
        <section class="col-lg-12">  
            <div class="box box-info">
                <div class="box-header with-border">
                    <br>
                    <h3>แก้ไขข้อมูลการติดต่อ</h3>
                    <?php
                      
                    ?>

                    <form class="form-horizontal" method="post" id="save_edit">
                      <div class="box-body">
                        <div class="form-group">
                  <!--        <label class="control-label col-sm-3" for="email">Email ปัจจุบัน:</label>
                          <div class="col-sm-4">
                            <?=$data_user['email'] ?>
                          </div>
                        </div>
                  -->
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">E-mail :</label>
                          <div class="col-sm-4">
                            <input type="email" class="form-control" name="email" placeholder="Enter email" value="<?=$data_user['email'] ?>">
                          </div>
                        </div>
                  <!--      
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="phonenumber">Phone Number ปัจจุบัน:</label>
                          <div class="col-sm-3">
                            <?=$data_user['phonenumber'] ?>
                            
                          </div>
                        </div>
                    -->

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="phonenumber">เบอร์โทรศัพท์ :</label>
                          <div class="col-sm-3">
                            <input type="tel" class="form-control" name="phonenumber" placeholder="Enter phone number" maxlength="14" value="<?=$data_user['phonenumber'] ?>">
                            ตัวอย่าง 0987654321
                          </div>
                        </div>
                        

                        <script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>
                        <script type="text/javascript">
                            var config = {
                             
                              '.chosen-select-deselect'  : {allow_single_deselect:true},
                           
                            }
                            for (var selector in config) {
                              $(selector).chosen(config[selector]);
                            }
                        </script>
                      </div>


                        <input id="id"  type="hidden" class="form-control"   name="id_customer" value="<?=$data_user['id']?>" >
                        <div class="box-footer clearfix">
                          <button type="submit" class="col-sm-2 pull-right btn btn-primary" id="edit">บันทึกการแก้ไข  <i class="fa fa-arrow-circle-right"></i></button>
                        </div>

                    </form>

                    </div><!-- /.box-header -->

            </div>
        </section>
    </div>
</section>

<script src="<?=base_url();?>/publicts/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="<?=base_url()?>/publicts/dashboard/plugins/resources/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
    
    $(document).ready(function() {
    $('#example22').DataTable();
    });
</script>