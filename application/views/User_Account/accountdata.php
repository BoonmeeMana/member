

<script type="text/javascript" src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">

<link rel="stylesheet" href="<?=base_url();?>publicts/chosen/chosen.css">


<style type="text/css" media="all">
    .chosen-rtl .chosen-drop { left: -9000px; }
  </style>
<script type="text/javascript">
  $( document ).ready(function() {

      $('#save_edit').submit(function(){
            $.post('<?=base_url().$this->router->class.'/save_edit'?>',$("form").serialize(),function( data ){

                  if(data.txt_status == true){
                      swal("บันทึกการแก้ไขสำเร็จ!", "กรุณาคลิกที่ปุ่ม!", "success");
                      setTimeout("redirect_page();",1000); 
                  }else if(data.txt_status == 0){
                      swal("Data is Double" , "error");
                  }else{
                      swal("Error "+data.txt_status,"Database Error "+data.txt_status, "error");
                  }

                  if(data.validation!=null){
                    
                    swal(data.validation, "error");
  
                  }

            },"json");
             
            return false;  
      });
        
  });

function redirect_page(){
    window.location = "<?=base_url().$this->router->class?>/index";
}
</script>


<section class="content-header">
          <h1>ส่วนควบคุมบัญชีผู้ใช้</h1>
          <ol class="breadcrumb">
            <li><a href="#"> Home</a></li>
            <li><a href="<?=base_url()?>User_Account/index"> Customer</a></li>
            <li class="active">User Account</li>
          </ol>
    </section>

<section class="content">
    <div class="row">
        <section class="col-lg-12">  
            <div class="box box-info">
                <div class="box-header with-border">
                    <br>
                    <h3>แก้ไขข้อมูลบัญชี</h3>
                    <?php
                    	
                    ?>

                    <form class="form-horizontal" method="post" id="save_edit">
                      <div class="box-body">
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="prefix">คำนำหน้า:</label>
                            <div class="col-sm-2">
                              <select class="form-control" name="prefix">
                                <option value="">คำนำหน้า</option>
                                  <?php
                                    foreach ($prefix as $key => $value) {
                                  ?>
                                <option <?=$value==@$data_user['prefix']? 'selected':''  ?> value="<?=$value; ?>">
                                    <?php echo $value; ?>
                                </option> 
                                    
                                  <?php
                                    }
                                ?>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="fullname">ชื่อ - สกุล:</label>
                          <div class="col-sm-3">
                            <input type="text" class="form-control" name="fullname" placeholder="Enter fullname" value="<?=$data_user['name']; ?>" required>
                          </div>
                        </div>
                
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="address">ที่อยู่:</label>
                          <div class="col-sm-4">
                            <textarea  rows="3" cols="40" name="address"><?=$data_user['address']?></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="Province">จังหวัด:</label>
                            <div class="col-sm-3">
                              <select style="width:250px;" class="chosen-select-deselect" tabindex="12" name="province">
                                <option value="">กรุณาเลือกจังหวัด</option>
                                  <?php
                                    foreach ($province as $key => $value) {
                                  ?>
                                    <option <?=$data_user['province_id']==@$value['id']? 'selected':''  ?> value="<?=$value['id']; ?>" required>
                                    <?php echo $value['province']; ?>
                                </option>

                                 
                                  <?php
                                    }
                                ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="postalcode">รหัสไปรษณีย์:</label>
                          <div class="col-sm-2">
                            <input type="text" class="form-control" name="postalcode" placeholder="Enter postalcode" value="<?=$data_user['postalcode']?>" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="birthdate">วันเกิด:</label>
                          <div class="col-sm-4">
                            <input id="birthdate" type="date" name="birthdate" onchange="calAge(this);" value="<?=$data_user['birthdate']?>" required>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="gender">เพศ:</label>
                          <div class="col-sm-3">
                            <label class="radio-inline">
                            <input type="radio" name="gender" value="Male" <?=$data_user['gender']=="Male"?'checked':'' ?>>  ชาย 
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="gender" value="Female" <?=$data_user['gender']=="Female"?'checked':'' ?>> หญิง 
                          </label>
                          </div>
                        </div>


                        <script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>
                        <script type="text/javascript">
                            var config = {
                             
                              '.chosen-select-deselect'  : {allow_single_deselect:true},
                           
                            }
                            for (var selector in config) {
                              $(selector).chosen(config[selector]);
                            }
                        </script>
                      </div>


                        <input id="id"  type="hidden" class="form-control"   name="id_customer" value="<?=$data_user['id']?>" >
                        <div class="box-footer clearfix">
                          <button type="submit" class="col-sm-2 pull-right btn btn-primary" id="edit">บันทึกการแก้ไข  <i class="fa fa-arrow-circle-right"></i></button>
                        </div>

                    </form>

                </div><!-- /.box-header -->

            </div>
        </section>
    </div>
</section>

<script src="<?=base_url();?>/publicts/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="<?=base_url()?>/publicts/dashboard/plugins/resources/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
    
    $(document).ready(function() {
    $('#example22').DataTable();
    });
</script>