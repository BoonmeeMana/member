

<script type="text/javascript" src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">

<link rel="stylesheet" href="<?=base_url();?>publicts/chosen/chosen.css">

<script type="text/javascript">
  /*$( document ).ready(function() {

      $('#save_change').submit(function(){
            $.post('<?=base_url().$this->router->class.'/save_change'?>',$("form").serialize(),function( data ){

                  if(data.txt_status == true){
                      swal("บันทึกการแก้ไขสำเร็จ!", "กรุณาคลิกที่ปุ่ม!", "success");
                      setTimeout("redirect_page();",1000); 
                  }else if(data.txt_status == 0){
                      swal("Data is Double" , "error");
                  }else{
                      swal("Error "+data.txt_status,"Database Error "+data.txt_status, "error");
                  }

                  if(data.validation!=null){
                    
                    swal(data.validation, "error");
  
                  }

            },"json");
             
            return false;  
      });
        
  });

function redirect_page(){
    window.location = "<?=base_url().$this->router->class?>/index";
}*/
</script>
<section class="content-header">
          <h1>ส่วนควบคุมบัญชีผู้ใช้</h1>
          <ol class="breadcrumb">
            <li><a href="#"> Home</a></li>
            <li><a href="<?=base_url()?>User_Account/index"> Customer</a></li>
            <li class="active">User Account</li>
          </ol>
    </section>

<section class="content">
    <div class="row">
        <section class="col-lg-12">  
            <div class="box box-info">
                <div class="box-header with-border">
                    <br>
                    <h3>เปลี่ยนรหัสผ่าน</h3>
                    <?php
                      if (@$error==true) {
                     ?>
                        <div class="alert alert-<?=@$error==true?'danger':'success'?> " style="width:750px;">
                            <?=@$error?>
                      </div>
                    <?php
                      }
                    ?>
                    <?php
                      if (@$password_fail==true) {
                     ?>
                        <div class="alert alert-<?=@$password_fail==true?'danger':'success'?> " style="width:750px;">
                            <?=@$password_fail?>
                      </div>
                    <?php
                    
                      }
                    ?>
                    
                    <form class="form-horizontal" method="post" action="<?=base_url()?>User_Account/save_change">
                      <div class="box-body">

                          <div class="form-group">
                          <label class="control-label col-sm-3" for="oldpassword">ยืนยันรหัสผ่านปัจจุบัน<font color="red">*</font> :</label>
                          <div class="col-sm-4">
                             <input type="password" class="form-control" name="oldpassword" value="" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="newpassword">รหัสผ่านใหม่<font color="red">*</font> :</label>
                          <div class="col-sm-4">
                             <input type="password" class="form-control" name="newpassword" value="" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="repassword">ยืนยันรหัสผ่านใหม่<font color="red">*</font> :</label>
                          <div class="col-sm-4">
                             <input type="password" class="form-control" name="repassword" value="" required>
                          </div>
                        </div>

                        <script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>
                        <script type="text/javascript">
                            var config = {
                             
                              '.chosen-select-deselect'  : {allow_single_deselect:true},
                           
                            }
                            for (var selector in config) {
                              $(selector).chosen(config[selector]);
                            }
                        </script>
                      </div>

                        <input type="hidden" class="form-control"   name="password" value="<?=$data_user['password'] ?>" >
                        <input id="id"  type="hidden" class="form-control"   name="id_customer" value="<?=$data_user['id'] ?>" >
                        <div class="box-footer clearfix">
                          <button type="submit" class="col-sm-2 pull-right btn btn-primary" id="edit">บันทึก <i class="fa fa-arrow-circle-right"></i></button>
                        </div>

                    </form>

                </div><!-- /.box-header -->

            </div>
        </section>
    </div>
</section>

<script src="<?=base_url();?>/publicts/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="<?=base_url()?>/publicts/dashboard/plugins/resources/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
    
    $(document).ready(function() {
    $('#example22').DataTable();
    });
</script>