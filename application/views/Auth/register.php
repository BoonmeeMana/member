<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
<title>Sign Up for Member</title>
<SCRIPT TYPE="text/javascript" SRC="<?=base_url();?>/publicts/css/bootstrap.min.css"></SCRIPT>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="<?=base_url();?>/publicts/chosen/chosen.css">
  <style type="text/css" media="all">

    .chosen-rtl .chosen-drop { left: -9000px; }
  </style>



<script language="javascript">
function calAge(o){
     var tmp = o.value.split("-");
     var current = new Date();
     var current_year = current.getFullYear();
     document.getElementById("age").value = current_year - tmp[0];
}
</script>

<style type="text/css">
	body {
		background-color: #AAA;
	}
	.panel {
		margin-top: 3%;
		margin-bottom: 5%;
		margin-left: 10%;
		margin-right: 10%;
		font-family: ‘Lucnamea Console’, Monaco, monospace;
		font-size: 15px;
	}
	
</style>
</head>
<body>


<div class="container">
	<br>
	
	<div class="panel panel-default">
		<center>
			<h1 style="font-family: Impact, Charcoal, sans-serif;"> MEMBER </h1>
			<p>sign up for use in company</p>
		</center>
		<div class="container">	
			<h2  style="font-weight: normal;"> สมัครสมาชิก </h2>
				<?php
				  if (@$error==true) {
				 ?>
				  	<div class="alert alert-danger " style="width:750px;">
					  		<?=@$error?>
					</div>
				<?php
					}
				?>
				<?php
				  if (@$double_user==true) {
				 ?>
				  	<div class="alert alert-danger " style="width:750px;">
					  		<?=@$double_user?>
					</div>
				<?php
					}
				?>


			<form class="form-horizontal" role="form" method="POST" action="<?=base_url().$this->router->class;?>/add">
				
				<div class="form-group">
			      <label class="control-label col-sm-2" for="prefix">คำนำหน้า:</label>
				      <div class="col-sm-2">
				        <select class="form-control" name="prefix">
				        	<option value="">เลือกคำนำหน้า</option>
				        		<?php
				        			foreach ($prefix as $key => $value) {
				        		?>
									<option <?=$value==@$check_prefix? 'selected':''  ?> value="<?=$value; ?>">
									    <?php echo $value; ?>
									</option> 
				        			
				        		<?php
				        			}
    							?>
					    </select>
				      </div>
			      <label class="control-label col-sm-2" for="fullname">ชื่อ - สกุล<font color="red">*</font>:</label>
			      <div class="col-sm-2">
			        <input type="text" class="form-control" name="fullname" placeholder="Enter fullname" value="<?=@$fullname?>">
			      </div>
			    </div>
			    
			    <div class="form-group">
			      <label class="control-label col-sm-2" for="email">E-mail<font color="red">*</font>:</label>
			      <div class="col-sm-2">
			        <input type="email" class="form-control" name="email" placeholder="Enter email" value="<?=@$email?>">
			      </div>
			      	
			      <label class="control-label col-sm-2" for="username">Username<font color="red">*</font>:</label>
			      <div class="col-sm-2">
			        <input type="text" class="form-control" name="username" placeholder="Enter username" value="<?=@$username?>">
			      </div>
			      
			    </div>
			    <div class="form-group">
			      <label class="control-label col-sm-2" for="password">Password<font color="red">*</font>:</label>
			      <div class="col-sm-2">          
			        <input type="password" class="form-control" name="pwd" placeholder="Enter password">
			       	
			      </div>
			    
			      <label class="control-label col-sm-2"  for="ccpassword">Re Enter Password<font color="red">*</font>:</label>
			      <div class="col-sm-2">          
			        <input type="password" class="form-control" name="ccpwd" placeholder=" Re Enter Password">
			      	
			      </div>
			    </div>
			    <div class="form-group">
			      <label class="control-label col-sm-2" for="address">ที่อยู่<font color="red">*</font>:</label>
			      <div class="col-sm-4">
			        <textarea  rows="3" cols="23" name="address"><?=@$address?></textarea>
			      </div>
			    </div>
			    <div class="form-group">
			      <label class="control-label col-sm-2" for="Province">จังหวัด<font color="red">*</font>:</label>
			      	<div class="col-sm-2">
				        <select style="width:165px;" class="chosen-select-deselect" tabindex="12" name="province">
				        	<option value="">กรุณาเลือกจังหวัด</option>
				        		<?php
				        			foreach ($province as $key => $value) {
				        		?>
				        			<option <?=$value['id']==@$check_province? 'selected':''  ?> value="<?=$value['id']; ?>">
									    <?php echo $value['province']; ?>
									</option>
				        			
				        		<?php
				        			}
    							?>
					    </select>
				    </div>
			    
			      <label class="control-label col-sm-2" for="postalcode">รหัสไปรษณีย์<font color="red">*</font>:</label>
			      <div class="col-sm-2">
			        <input type="text" class="form-control" name="postalcode" placeholder="Enter postalcode" value="<?=@$postalcode?>">
			      </div>
			    </div>
			    <div class="form-group">
			      <label class="control-label col-sm-2" for="phonenumber">เบอร์โทรศัพท์<font color="red">*</font>:</label>
			      <div class="col-sm-3">
			        <input type="tel" style="width:165px;" class="form-control" name="phonenumber" placeholder="Enter phone number" maxlength="14" value="<?=@$phonenumber?>">
			        ตัวอย่าง 0987654321
			      </div>
			    
			      <label class="control-label col-sm-1" for="birthdate">วันเกิด:</label>
			      <div class="col-sm-4">
			        <input id="birthdate" style="width:165px;" type="date" name="birthdate" onchange="calAge(this);" value="<?=@$birthdate?>">
			      </div>
			    </div>
			    <div class="form-group">
			    	<label class="control-label col-sm-2" for="gender">เพศ:</label>
			      <div class="col-sm-3">
			        <label class="radio-inline">
				      <input type="radio" name="gender" value="Male" checked>ชาย
				    </label>
				    <label class="radio-inline">
				      <input type="radio" name="gender" value="Female">หญิง
				    </label>
			      </div>

			    </div>
			    <div id="content">
</div>
			    <div class="form-group">        
			      <div class="col-sm-offset-2 col-sm-10">
			      	
			        <button type="submit" name="btnsave" class="btn btn-primary" style="width:250px;">สมัคร</button>
			      	&nbsp;
			        <button type="reset" class="btn" style="width:250px;background-color: #AAA">Reset</button>
			      	
			      </div>
			    </div>


				    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
					  <script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>
					  <script type="text/javascript">
					    var config = {
					     
					      '.chosen-select-deselect'  : {allow_single_deselect:true},
					   
					    }
					    for (var selector in config) {
					      $(selector).chosen(config[selector]);
					    }
					</script>


			</form>
			
		</div>
	</div>
	
</div>
</body>
</html>