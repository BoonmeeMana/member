
<link rel="stylesheet" href="<?=base_url();?>/publicts/chosen/chosen.css">
<script type="text/javascript" src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">

    <script type="text/javascript">

    if("<?=$this->session->userdata('txt_status')?>"){
    //  swal("Good job!", "You clicked the button!", "success");
      swal("แก้ไขสำเร็จ!", "กรุณาคลิกที่ปุ่ม!", "success");
      <?php
      $this->session->unset_userdata('txt_status');

      ?>
    }
        
    function delete_newsletter(id){

      swal({  title:  "คุณแน่ใจใช่มั้ย?",   
              text:   "คุณจะไม่สามารถกู้คืนไฟล์นี้!", 
                
              type:   "warning",   
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55", 
              closeOnConfirm: false 
            }, 
          
        function(){           
          $.post("<?=base_url().$this->router->class.'/delete'?>",{'id':id}).done(
                function(data){
                  if(data=="TRUE"){
                    swal("ลบจดหมายข่าวนี้สำเร็จ");               
                    setTimeout("redirect();",1000);                   
                  }else{
                        sweetAlert("error");
                  }
                }
          );

        });

  }

  function redirect(){
      window.location = "<?=base_url().$this->router->class?>";
  }

        function edit(id){
            $.post("<?=base_url().$this->router->class.'/edit'?>",{'id':id}).done(function(){
                window.location = "<?=base_url().$this->router->class.'/edit'?>";
            }); 
        }

    </script>

    <section class="content-header">
          <h1>จดหมายข่าว</h1>
          <ol class="breadcrumb">
            <li><a href="#"> Home</a></li>
            <li><a href="<?=base_url()?>Newsletter/index"> Newsletter</a></li>
            <li class="active">View newsletter</li>
          </ol>
    </section>

<section class="content">
    <div class="row">
        <section class="col-lg-12">  
            <div class="box box-info">
                <div class="box-header with-border">
                    <h2 class="box-title">แสดงจดหมายข่าว</h2>

                    <table id="example22" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th  style="text-align: center; background-color:#C1CDCD;width:1px;">No.</th>
                                <th class= "code" style="text-align: center;background-color:#C1CDCD;width:40px;">หัวข้อข่าว</th>
                                <th class= "code" style="text-align: center;background-color:#C1CDCD;width:120px;">จดหมายข่าว</th>
                                <th class= "code" style="text-align: center;background-color:#C1CDCD;width:50px;">รายละเอียด</th>
                                <th class= "edit no-sort" style='text-align: center; background-color:#FFFACD;width:30px;'>แก้ไข</th>
                                <th class= "delete no-sort" style='text-align: center; background-color:#FF9999;width:30px;'>ลบจดหมายข่าว</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i=1;
                                foreach (@$newsletter as $key => $value) {
                            ?>
                            <tr>
                                <td style="text-align: center;"><?=$i++?></td>
                                <td style="text-align: center;"><?=$value['category_name']; ?></td>
                                <td style="text-align: center;"><?=$value['newsletter_name']; ?></td>
                                <td style="text-align: center;">
                                  <a class="iframe" href="<?=base_url()?>Newsletter/popup_detail/<?=$value['id']?>"><font face="Times New Roman" size="3">ดูรายละเอียด</font></a></p>
                                </td>
                                <td style="text-align: center;"><a href="javascript:edit(<?=$value['id']?>);" ><font face="Times New Roman" size="3">แก้ไข</font></a></td>
                                <td style="text-align: center;" onclick=" delete_newsletter(<?=$value['id']?>)"><a  href="#" ><font face="Times New Roman" size="3">ลบ</font></a></td>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>

                    </table>

                    </div><!-- /.box-header -->

            </div>
        </section>
    </div>
</section>

<script src="<?=base_url();?>/publicts/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="<?=base_url()?>/publicts/dashboard/plugins/resources/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
    
    $(document).ready(function() {
    $('#example22').DataTable();
    });
</script>

<script src="<?=base_url();?>/publicts/iframepopup/jquery.colorbox.js"></script>
<script>
      $(document).ready(function(){

        $(".iframe").colorbox({
          iframe:true, 
          width:"75%", 
          height:"100%"
        });

      });
    </script> 


