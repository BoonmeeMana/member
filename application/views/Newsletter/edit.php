<script type="text/javascript" src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">

  <style type="text/css" media="all">

    .chosen-rtl .chosen-drop { left: -9000px; }
  </style>

<script src="<?=base_url();?>/publicts/editor/assets/js/jquery.min.js"></script>
<script src="<?=base_url();?>/publicts/editor/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
  tinymce.init({
      selector: "textarea",theme: "modern",height: 300,
         relative_urls : false,
         remove_script_host: false,
      plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
         "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
      ],
      
     toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
     toolbar2: "| responsivefilemanager | code | link unlink anchor | image media | forecolor backcolor  | print preview  ",
     image_advtab: true ,
   
     external_filemanager_path:"<?=base_url()?>publicts/editor/plugins/tinymce/plugins/filemanager/",
     filemanager_title:"Responsive Filemanager" ,
     external_plugins: { "filemanager" : "../tinymce/plugins/filemanager/plugin.min.js"}
 });
</script>


    <section class="content-header">
          <h1>จดหมายข่าว</h1>
          <ol class="breadcrumb">
            <li><a href="#"> Home</a></li>
            <li><a href="<?=base_url()?>Newsletter/index"> Newsletter</a></li>
            <li class="active">edit newsletter</li>
          </ol>
    </section>

<section class="content">
    <div class="row">
        <section class="col-lg-12">  
            <div class="box box-info">
              <div class="box-header with-border">
                <h2 class="box-title">แบบฟอร์มแก้ไขจดหมายข่าว</h2>
              </div><!-- /.box-header -->

             <!-- form start -->
              <form class="form-horizontal" method="post" action="<?=base_url().$this->router->class.'/save_edit'?>">
                <div class="box-body">
                  <?php
            //      	print_r($category);
            /*        if (@$error==true) {
                   ?>
                      <div class="alert alert-danger " style="width:750px;">
                          <?=@$error?>
                    </div>
                  <?php
                    }*/
               

                  ?>
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="category">หัวข้อจดหมายข่าว</label>
                    <div class="col-sm-9">
                      <select style="width:250px;" class="chosen-select-deselect" tabindex="12" name="category">
		                  <option value="">เลือกประเภท Newsletter</option>
		                    <?php
		                      foreach ($category as $key => $value) {
		                    ?>
		                      <option <?=$data['category_name']==@$value['category_name']?"selected":''  ?> value="<?=$value['id']; ?>">
		                      <?php echo $value['category_name']; ?>
		                  </option>
		                      
		                    <?php
		                      }
		                  ?>
		              </select>
		              
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-2" class="control-label col-sm-1" for="newsletter_name">ชื่อจดหมายข่าว</label>
                    <div class="col-sm-10">
                      <input id="newsletter_name" type="text" class="form-control"   name="newsletter_name" value="<?=$data['newsletter_name']?>" >
                    </div>
                  </div>

                  <div class="form-group">
                          <label class="control-label col-sm-2" for="status">สถานะ</label>
                          <div class="col-sm-3">
                            <label class="radio-inline">
                            <input type="radio" name="status" value="0" <?=$data['newsletter_status']=="0"?'checked':'' ?>>  OFF
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="status" value="1" <?=$data['newsletter_status']=="1"?'checked':'' ?>> ON 
                          </label>
                          </div>
                        </div>
                  
                  <div class="form-group">
                    <label  class="col-sm-2 control-label">รายละเอียด</label>
                    <div class="col-sm-10">
                      <textarea name="newsletter_content"><?=$data['newsletter_content'];?></textarea>
                      </div>
                  </div>
                  
                </div>
                <input id="id_newsletter"  type="hidden" class="form-control"   name="id_newsletter" value="<?=$data['id']?>" >
                <div class="box-footer clearfix">
                  <div class="col-sm-12">
                    <button class="col-sm-3 pull-right btn btn-primary" id="edit_newsletter">บันทึกการแก้ไข <i class="fa fa-arrow-circle-right"></i></button>
                  </div>
                </div>

                <script src="<?=base_url();?>/publicts/chosen/chosen.jquery.js" type="text/javascript"></script>
                <script type="text/javascript">
                    var config = {
                     
                      '.chosen-select-deselect'  : {allow_single_deselect:true},
                   
                    }
                    for (var selector in config) {
                      $(selector).chosen(config[selector]);
                    }
                </script>

                  
              </form>
            </div><!-- /.box -->

        </section>
    </div>
</section>

