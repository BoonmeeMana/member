<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class province_model extends CI_Model{
  
	public function get() {
		$query = $this->db->get('province');
  		return $query->result_array();
	}

}