<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_Model extends CI_Model{

    public function __construct() {
            parent::__construct();
    }
  
	
    public function get() {
        $this->db->select('
                            province.province as province,
                            users.id,
                            users.prefix,
                            users.name,
                            users.email,
                            users.address,
                            users.postalcode,
                            users.phonenumber,
                            users.gender,
                            users.registerdate,
                            users.province_id,
                            users.type_user
                        ');
        $this->db->join('province','province.id = users.province_id');
        $this->db->order_by('users.id',"ASC");
        $query = $this->db->get('users');

        return $query->result_array();
    }

	public function store(){

        $username   = $this->add_account($this->input->post('username'));

        $pwd        = $this->input->post('pwd');
        $ccpwd      = $this->input->post('ccpwd');
       
        if($username){
            return "User นี้มีผู้ใช้แล้ว";
        }else if($pwd!=$ccpwd){
            return "รหัสผ่านไม่ตรงกัน";
        }else{
            $data = array(
                'prefix'            =>  trim($this->input->post('prefix')),
                'name'              =>  trim($this->input->post('fullname')),
                'email'             =>  trim($this->input->post('email')),
                'username'          =>  trim($this->input->post('username')),
                'password'          =>  trim($this->input->post('pwd')),
                'address'           =>  trim($this->input->post('address')),
                'postalcode'        =>  trim($this->input->post('postalcode')),
                'phonenumber'       =>  trim($this->input->post('phonenumber')),
                'birthdate'         =>  date('Y-m-d h:i:sa'),
                'gender'            =>  trim($this->input->post('gender')),
                'registerdate'      =>  date('Y-m-d h:i:sa'),
                'modifydate'        =>  date('Y-m-d h:i:sa'),
                'province_id'       =>  trim($this->input->post('province'))
            );
            
             $this->db->insert('users',$data);
        }		
	}

    public function check_login($user,$pass){

        
        $this->db->where('username', $user);
        $this->db->where('password', $pass);
        $this->db->limit(1);
        $query = $this->db->get('users');
        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
    }

      
     public function add_account($data) {

        $this->db->where('username',$data);
        $this->db->limit(1);
         $query = $this->db->get('users');
        return $query->result();
    }

    public function edit($id){
        
        $this->db->where('id',$id);
        $this->db->order_by('id',"DESC");
        $query = $this->db->get('users');
        return $query->row_array();
    }

    public function save_edit(){
   
        $this->db->where('id', $this->input->post('id'));
        $query = $this->db->get('users');
        $count_row = $query->num_rows();

        if ($count_row > 0) {
            return FALSE;
        }else{
            $data = array(                
                'prefix'            =>  trim($this->input->post('prefix')),
                'name'              =>  trim($this->input->post('fullname')),
                'email'             =>  trim($this->input->post('email')),
                'address'           =>  trim($this->input->post('address')),
                'postalcode'        =>  trim($this->input->post('postalcode')),
                'phonenumber'       =>  trim($this->input->post('phonenumber')),
                'type_user'         =>  trim($this->input->post('type')),
                'birthdate'         =>  date('Y-m-d h:i:sa'),
                'modifydate'        =>  date('Y-m-d h:i:sa'),
                'province_id'       =>  trim($this->input->post('province'))
                
                );
      
        $this->db->where('id',trim($this->input->post('id_customer')));
        return $this->db->update('users',$data);
        }
    }

    public function save_edit_account(){
   
        $this->db->where('id', $this->input->post('id'));
        $query = $this->db->get('users');
        $count_row = $query->num_rows();

        if ($count_row > 0) {
            return FALSE;
        }else{
            $data = array(                
                'prefix'            =>  trim($this->input->post('prefix')),
                'name'              =>  trim($this->input->post('fullname')),
                'address'           =>  trim($this->input->post('address')),
                'postalcode'        =>  trim($this->input->post('postalcode')),
                'gender'            =>  trim($this->input->post('gender')),
                'birthdate'         =>  date('Y-m-d h:i:sa'),
                'modifydate'        =>  date('Y-m-d h:i:sa'),
                'province_id'       =>  trim($this->input->post('province'))
                
                );
      
        $this->db->where('id',trim($this->input->post('id_customer')));
        return $this->db->update('users',$data);
        }

    }

    public function save_edit_contact(){
   
        $this->db->where('id', $this->input->post('id'));
        $query = $this->db->get('users');
        $count_row = $query->num_rows();

        if ($count_row > 0) {
            return FALSE;
        }else{
            $data = array(
                'email'             =>  trim($this->input->post('email')),
                'phonenumber'       =>  trim($this->input->post('phonenumber')),
                'modifydate'        =>  date('Y-m-d h:i:sa')
                
                );
      
        $this->db->where('id',trim($this->input->post('id_customer')));
        return $this->db->update('users',$data);
        }

    }

    public function delete(){

        $id = $this->session->userdata('delete_customer');
        $this->db->where('id', $id);
        return $this->db->delete('users');
    }

    public function get_type_user(){
        $this->db->where('id',$this->session->userdata('logged_in')['id']);
        $query = $this->db->get('users');
        return $query->row();
    }

    public function get_data_user(){
        
        $this->db->where('users.id',$this->session->userdata('logged_in')['id']);

        $this->db->select('
                            province.province as province,
                            users.id,
                            users.prefix,
                            users.name,
                            users.email,
                            users.address,
                            users.postalcode,
                            users.phonenumber,
                            users.birthdate,
                            users.gender,
                            users.password,
                            users.province_id,
                            users.type_user
                        ');
        $this->db->join('province','province.id = users.province_id');
        $this->db->order_by('users.id',"ASC");

        
        $query = $this->db->get('users');
 
        return $query->row_array();
    }
    
    public function save_follownews(){
        $this->db->where('users_id',trim($this->input->post('id_customer')));

        if ($this->input->post('follownews') == null) {
            return $this->db->delete('follownews');
        }else {
            if ($this->db->delete('follownews') == true) {
                foreach (@$this->input->post('follownews') as $key => $value) {
                        $data = array(                
                            'follownews_name'   =>  trim($this->input->post("follownews[$key]")),
                            'follownews_created'      =>  date('Y-m-d h:i:sa'),
                            'follownews_updated'        =>  date('Y-m-d h:i:sa'),
                            'users_id'          =>  trim($this->input->post('id_customer'))
                        );
                        $this->db->insert('follownews',$data);
                }
            } 
        }
               
    }

    public function get_follownews() {
        $this->db->where('users_id',$this->session->userdata('logged_in')['id']);
        $query = $this->db->get('follownews');

        return $query->result_array();
    }

    public function save_change(){

        $this->db->where('id',$this->session->userdata('logged_in')['id']);
        $pwd           = $this->input->post('password');
        $oldpwd        = $this->input->post('oldpassword');
        
        $newpwd        = $this->input->post('newpassword');
        $ccpwd         = $this->input->post('repassword');

       
        if($pwd!=$oldpwd){
            
            return "รหัสผ่านไม่ตรงกับรหัสผ่านปัจจุบัน";
    
        }else if($newpwd!=$ccpwd){
            return "รหัสผ่านใหม่ไม่ตรงกัน";
        }else{
            $data = array(
                'password'          =>  trim($this->input->post('newpassword'))
            );
            
            $this->db->update('users',$data);
            
        }
    }

    public function get_follow_send() {
        $this->db->select('
                            
                            follownews.follownews_id,
                            follownews.follownews_name,
                            follownews.users_id,
                            users.email as email,
                            category.category_name,
                            category.id,
                            newsletter.newsletter_name,
                            newsletter.newsletter_content
                            
                        ');
        $this->db->join('users','users.id = follownews.users_id');
        $this->db->join('category','category.category_name = follownews.follownews_name');
        $this->db->join('newsletter','newsletter.category_id = category.id');
        $query = $this->db->get('follownews');
        return $query->result_array();
    }
  
  
}