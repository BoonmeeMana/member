<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model{

    public function __construct() {
            parent::__construct();
    }

    public function get() {
        $query = $this->db->get('category');

         return $query->result_array();

    }

    public function edit($id){
        
        
        $this->db->where('id',$id);
        $this->db->order_by('id',"ASC");
        $query = $this->db->get('category');
        return $query->row_array();
    }

	public function store(){

            $data = array(
                'category_name'     =>  trim($this->input->post('category')),
                'created_at'        =>  date('Y-m-d h:i:sa'),
                'updated_at'        =>  date('Y-m-d h:i:sa')   
            );
            
             $this->db->insert('category',$data);
        
		
	}

    public function save_edit(){

        $this->db->where('id', $this->input->post('id'));
        $query = $this->db->get('category');
        $count_row = $query->num_rows();

        if ($count_row > 0) {
            return FALSE;
        }else{
            $data = array(
                    'category_name'       =>  trim($this->input->post('category_name')),
                    'updated_at'        =>  date('Y-m-d h:i:sa') 
                );
      
            $this->db->where('id',trim($this->input->post('id_category')));
            return $this->db->update('category',$data);
        }

            
    }

    public function delete(){

        $id = $this->session->userdata('delete_category');
        $this->db->where('id', $id);
        return $this->db->delete('category');
    }
       
}