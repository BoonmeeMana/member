<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter_model extends CI_Model{

    public function __construct() {
            parent::__construct();
    }

    public function get() {
        $this->db->select('
                            category.category_name as category_name,
                            newsletter.id,
                            newsletter.newsletter_name,
                            newsletter.newsletter_status,
                            newsletter.newsletter_content,
                            newsletter.category_id
                        ');
        $this->db->join('category','category.id = newsletter.category_id');
        $this->db->order_by('newsletter.id',"ASC");
        $query = $this->db->get('newsletter');

        return $query->result_array();

    }

    public function edit($id){
        $this->db->select('
                            category.category_name as category_name,
                            newsletter.id,
                            newsletter.newsletter_name,
                            newsletter.newsletter_status,
                            newsletter.newsletter_content,
                            newsletter.category_id
                        ');
        $this->db->join('category','category.id = newsletter.category_id');
        $this->db->where('newsletter.id',$id);
        $this->db->order_by('newsletter.id',"DESC");
        $query = $this->db->get('newsletter');
        return $query->row_array();
    }


    public function save_edit(){


        $this->db->where('id', $this->input->post('id'));
        $query = $this->db->get('newsletter');
        $count_row = $query->num_rows();

        if ($count_row > 0) {
            return FALSE;
        }else{
            $data = array(
                'newsletter_name'       =>  trim($this->input->post('newsletter_name')),
                'newsletter_content'    =>  $this->input->post('newsletter_content'),
                'newsletter_status'     =>  trim($this->input->post('status')),
                'updated_at'            =>  date('Y-m-d h:i:sa'),
                'category_id'           =>  trim($this->input->post('category'))
                );
      
        $this->db->where('id',trim($this->input->post('id_newsletter')));
        return $this->db->update('newsletter',$data);
        }

    }

    public function delete(){
        $id = $this->session->userdata('delete_newsletter');
        $this->db->where('id', $id);
        return $this->db->delete('newsletter');
    }

	public function store(){


            $data = array(
                'newsletter_name'       =>  trim($this->input->post('subject')),
                'newsletter_content'    =>  $this->input->post('newsletter_content'),
                'created_at'            =>  date('Y-m-d h:i:sa'),
                'updated_at'            =>  date('Y-m-d h:i:sa'),
                'category_id'           =>  trim($this->input->post('category'))
                
            );
            
             $this->db->insert('newsletter',$data);

	}

    public function get_popup($id){
         $this->db->where('newsletter.id',$id);
         $query = $this->db->get('newsletter');
            return $query->row_array();
         
    }
       
    public function get_change_news($id){
        $this->db->where('category_id',$id);
        $query = $this->db->get('newsletter');
        return $query->result_array();
    }
}