<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Point_model extends CI_Model{

    public function __construct() {
            parent::__construct();
    }

    public function get() {
        $query = $this->db->get('rewardpoint');
        return $query->result_array();
    }

    public function get_point() {
        $this->db->select('
                            users.name as name,
                            rewardpoint.id,
                            rewardpoint.rewardpoint_category,
                            rewardpoint.rewardpoint_name,
                            rewardpoint.rewardpoint_score,
                            rewardpoint.users_id,
                            users.type_user
                        ');
        $this->db->join('users','users.id = rewardpoint.users_id');
        $this->db->order_by('rewardpoint.id',"ASC");
        $query = $this->db->get('rewardpoint');

        return $query->result_array();
    }

    public function delete(){
        $id = $this->session->userdata('delete_point');
        $this->db->where('id', $id);
        return $this->db->delete('rewardpoint');
    }

}